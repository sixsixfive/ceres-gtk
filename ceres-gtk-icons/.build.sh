#!/bin/sh
set -e
_basedir="$(dirname "$(readlink -f "${0}")")"
cd "$_basedir"
gtk-update-icon-cache -f --include-image-data "$_basedir"
