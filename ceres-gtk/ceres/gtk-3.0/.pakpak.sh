#!/bin/sh
#packs and unpacks the gtk resource file
#packs only css files not svgs or pngs!
set -e
_basedir="$(dirname "$(readlink -f "${0}")")"
cd "$_basedir"
if [ -d "$_basedir/theme" ]; then
	printf "packing....\n"
	if type glib-compile-resources >/dev/null 2>&1; then
		printf "glib-compile-resources found\n"
	else
		printf "glib-compile-resources not found - Aborting!\n"
		exit 1
	fi
	if type xmllint >/dev/null 2>&1; then
		printf "xmllint found\n"
	else
		printf "xmllint not found - Aborting!\n"
		exit 1
	fi
	if type find >/dev/null 2>&1; then
		printf "find found\n"
	else
		printf "find not found - Aborting!\n"
		exit 1
	fi
	if type sed >/dev/null 2>&1; then
		printf "sed found\n"
	else
		printf "sed not found - Aborting!\n"
		exit 1
	fi
	cat <<\EOF > "$_basedir/"gtk.gresource.xml
<?xml version="1.0" encoding="UTF-8"?>
<gresources>
<gresource prefix="/org/gtk/libgtk/">
EOF
	printf "<file>gtk-main.css</file>\n" >> "$_basedir/"gtk.gresource.xml
	for _f in $(find theme -name "*.css"); do
		printf "<file>$_f</file>\n" >> "$_basedir/"gtk.gresource.xml
	done
	printf "</gresource>\n</gresources>\n" >> gtk.gresource.xml
	glib-compile-resources "$_basedir/"gtk.gresource.xml
	if [ -f "$_basedir/"gtk.gresource ]; then
		rm "$_basedir/"gtk.gresource.xml "$_basedir/"gtk-main.css
		rm -rf "$_basedir/"theme 
		sed -i '/@import url'\('"gtk-main.css"'\)';/d' gtk.css
		sed -i '/@import url'\('"gtk-main.css"'\)';/d' gtk-dark.css
	fi
else
	printf "unpacking....\n"
	_gr_file="$_basedir/gtk.gresource"
	_gr_basedir="/org/gtk/libgtk/"
	for _required_prog in gresource; do
		type ${_required_prog} &>/dev/null 2>&1
		if [ "$?" -ne "0" ]; then
			printf "Unable to find required program '${_required_prog}' in PATH.\n"
			exit 1
		fi
	done
	for _rsrc in $(gresource list $_gr_file)
	do
		_rsrc_file=$(echo "${_rsrc#$_gr_basedir}")
		mkdir -p $(dirname "$_rsrc_file") ||:
		gresource extract "$_gr_file" "$_rsrc" > "$_rsrc_file"
	done
	if [ -d "$_basedir/theme" ]; then
		rm "$_basedir/"gtk.gresource
		printf '@import url("gtk-main.css");\n' >> gtk.css
		printf '@import url("gtk-main.css");\n' >> gtk-dark.css
	fi
fi
