#!/bin/sh
#builds a cinnamon.css from varios css files to avoid the one big file mess....
set -e
_basedir="$(dirname "$(readlink -f "${0}")")"
cd "$_basedir"
if [ -f $_basedir/../../cinnamon.css ]; then
	rm -f $_basedir/../../cinnamon.css
fi
if [ -f "$_basedir/build.sh" ]; then
  for CSS in $(ls shell|grep .css$); do 
    printf "/*  \n  $CSS  \n*/\n" >> $_basedir/../../cinnamon.css
    cat $_basedir/shell/$CSS >> $_basedir/../../cinnamon.css
    printf "\n">> $_basedir/../../cinnamon.css
  done
  for CSS in $(ls applets|grep .css$); do 
    printf "/*  \n  $CSS  \n*/\n" >> $_basedir/../../cinnamon.css
    cat $_basedir/applets/$CSS >> $_basedir/../../cinnamon.css
    printf "\n">> $_basedir/../../cinnamon.css
  done
  for CSS in $(ls desklets|grep .css$); do 
    printf "/*  \n  $CSS  \n*/\n" >> $_basedir/../../cinnamon.css
    cat $_basedir/desklets/$CSS >> $_basedir/../../cinnamon.css
    printf "\n">> $_basedir/../../cinnamon.css
  done
fi
if [ -f $_basedir/../../cinnamon.css ]; then
	printf "\nSuccess!\n"
else
	printf "\nFailed!\n"
	exit 1
fi
#if [ -L /usr/share/themes/ceres-pro-test ]; then
#	gsettings set org.cinnamon.theme name "cinnamon"
#	gsettings set org.cinnamon.theme name "ceres"
#fi
