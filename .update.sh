#!/bin/sh
set -e
_basedir="$(dirname "$(readlink -f "${0}")")"
cd "$_basedir"
if [ -d "$_basedir"/ceres-gtk/ceres/gtk-3.0/theme ]; then
	cd "$_basedir"/ceres-gtk/ceres/gtk-3.0
	sh .pakpak.sh
	cd "$_basedir"
fi
if [ ! -f "$_basedir"/ceres-gtk/ceres-pro/cinnamon/cinnamon.css ]; then
	cd "$_basedir"/ceres-gtk/ceres-pro/cinnamon/theme/.src
	sh build.sh
	cd "$_basedir"
fi
if [ ! -f "$_basedir"/ceres-gtk/ceres/cinnamon/cinnamon.css ]; then
	cd "$_basedir"/ceres-gtk/ceres/cinnamon/theme/.src
	sh build.sh
	cd "$_basedir"
fi
if type gtk-update-icon-cache >/dev/null 2>&1; then
	cd "$_basedir"/ceres-gtk-icons
	sh .build.sh
	cd "$_basedir"
fi
LANG=C
MESSAGE="$(date -u '+%Y%m%d%H%M%S')"
cp -f packaging/debian/debian/changelog_template packaging/debian/debian/changelog
sed -i s'/__COMMIT__/'$MESSAGE'/g' packaging/debian/debian/changelog
cp -f packaging/suse/.buildsuserpm.sh.template packaging/suse/buildsuserpm.sh
sed -i s'/__COMMIT__/'$MESSAGE'/g' packaging/suse/buildsuserpm.sh

#building of packages
if type dh_testdir >/dev/null 2>&1; then
	cd "$_basedir"/packaging
	sh build_deb.sh
	mv ceres-theme-gtk_2.*_all.deb ceres-gtk-current.deb
fi
if type rpmbuild >/dev/null 2>&1; then
	cd "$_basedir"/packaging
	sh build_rpm.sh
	mv ceres-theme-gtk-2.*.noarch.rpm ceres-gtk-current.rpm
fi

cd "$_basedir"
git add .
git commit -m "$MESSAGE"
git push origin master
printf "\n...done\n"
exit 0
