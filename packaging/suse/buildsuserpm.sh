#!/bin/bash
set -e
_dir=`dirname "$(readlink -f "${0}")"`
_basedir=${_dir}
cd ${_basedir}
if [ -f ${_basedir}/../ceres-theme-gtk*.rpm ]; then
    rm -f ${_basedir}/../ceres-theme-gtk*.rpm
fi
if [ -f ${_basedir}/../ceres-gtk-current.rpm ]; then
    rm -f ${_basedir}/../ceres-gtk-current.rpm
fi
if [ -f ${_basedir}/ceres-theme-gtk.spec ]; then
    rm -f ${_basedir}/ceres-theme-gtk.spec
fi
if [ -d ${_basedir}/ceres-theme-gtk ]; then
    rm -rf ${_basedir}/ceres-theme-gtk
fi
	mkdir -p ${_basedir}/ceres-theme-gtk/usr/share/themes
	cp -R ${_basedir}/../../ceres-gtk/* ${_basedir}/ceres-theme-gtk/usr/share/themes
	mkdir -p ${_basedir}/ceres-theme-gtk/usr/share/icons
	cp -R ${_basedir}/../../ceres-gtk-icons ${_basedir}/ceres-theme-gtk/usr/share/icons
#mate
#	mkdir -p ${_basedir}/ceres-theme-gtk/usr/share/mate-panel/layouts
#	cp -R ${_basedir}/../../ceres-extra/mate-panel/layouts/* ${_basedir}/ceres-theme-gtk/usr/share/mate-panel/layouts
#	mkdir -p ${_basedir}/ceres-theme-gtk/usr/share/pixmaps/mate-window-applets
#	cp -R ${_basedir}/../../ceres-extra/mate-window-applets/* ${_basedir}/ceres-theme-gtk/usr/share/pixmaps/mate-window-applets
#	mkdir -p ${_basedir}/ceres-theme-gtk/usr/share/icons/mate-window-applets
#	cp -R ${_basedir}/../../ceres-extra/mate-window-applets/* ${_basedir}/ceres-theme-gtk/usr/share/icons/mate-window-applets
	mkdir -p ${_basedir}/ceres-theme-gtk/usr/share/mate-background-properties
	cp ${_basedir}/../../ceres-extra/mate-backgrounds/*.xml ${_basedir}/ceres-theme-gtk/usr/share/mate-background-properties
	mkdir -p ${_basedir}/ceres-theme-gtk/usr/share/backgrounds/ceres
	cp ${_basedir}/../../ceres-extra/mate-backgrounds/*.png ${_basedir}/ceres-theme-gtk/usr/share/backgrounds/ceres
#cinnamon, gnome & co
	mkdir -p ${_basedir}/ceres-theme-gtk/usr/share/gnome-background-properties
	cp ${_basedir}/../../ceres-extra/gnome-backgrounds/*.xml ${_basedir}/ceres-theme-gtk/usr/share/gnome-background-properties

##### REMOVE unwanted files
	rm -rf ${_basedir}/ceres-theme-gtk/usr/share/themes/ceres/cinnamon/theme/.src
	rm -rf ${_basedir}/ceres-theme-gtk/usr/share/themes/ceres-pro/cinnamon/theme/.src
	rm -f ${_basedir}/ceres-theme-gtk/usr/share/themes/ceres/metacity-1/.old.tgz
	rm -f ${_basedir}/ceres-theme-gtk/usr/share/themes/ceres/gtk-3.0/.pakpak.sh
	rm -f ${_basedir}/ceres-theme-gtk/usr/share/icons/ceres-gtk-icons/.build.sh

#creating the spec file:
cat <<\EOFALL> ${_basedir}/ceres-theme-gtk.spec
Buildroot: BUILDROOT
Name: ceres-theme-gtk
Version: 2.20210723083329
Release: 1
Summary: Ceres GTK 3 themes
License: BSD 2 Clause
Requires: google-noto-fonts, noto-mono-fonts, libgtk-3-0
Provides: ceres-theme-gtk = %version
Obsoletes: ceres-theme-gtk <= %version, ceres-theme-gtk-old 
Recommends: marco, muffin
Group: System/X11
BuildArch: noarch

%define _rpmdir ../
%define _unpackaged_files_terminate_build 0
%define _source_payload w6.xzdio
%define _binary_payload w6.xzdio
%description
Ceres theme

%post

%postun

%files
%defattr(-,root,root,-)
#%doc COPYING ReadMe.md LICENSE
%{_datadir}/*
EOFALL

rpmbuild -bb --buildroot=${_basedir}/ceres-theme-gtk ${_basedir}/ceres-theme-gtk.spec
if [ -f ${_basedir}/../noarch/ceres-theme-gtk*.rpm ]; then
	mv ${_basedir}/../noarch/ceres-theme-gtk*.rpm ${_basedir}/..
	rm -rf ${_basedir}/../noarch ${_basedir}/ceres-theme-gtk.spec
fi
