Ceres is a traditional and boring theme for MATE and Cinnamon.

[![Preview](https://gitlab.com/sixsixfive/ceres-gtk/raw/master/.preview.small.png)](https://gitlab.com/sixsixfive/ceres-gtk/raw/master/.preview.png)

### Features:

* Includes a GTK+3 theme and one that works without composite(eg for Fluxbox, IceWM etc.).
* Includes a GTK+2 theme that is only based on default gtk engines.
* Includes a GTK1 color scheme.
* Includes [Muffin](https://github.com/linuxmint/muffin)/[Marco](https://mate-desktop.org) themes.
* Includes [Cinnamon](https://github.com/linuxmint/Cinnamon) themes.
* Optionally: [IceWM](https://gitlab.com/sixsixfive/ceres-icewm) themes.
* Optionally: [Wallpaper patterns](https://gitlab.com/sixsixfive/ceres-gtk/-/tree/master/ceres-extra/mate-backgrounds).
* Optionally: [Chomium scrollbars](https://gitlab.com/sixsixfive/ceres-gtk/-/tree/master/ceres-extra/chromium/ceres).

#### Dependencies:

* [Noto font](https://www.google.com/get/noto/) (fallback font for all themes)
* [DMZ cursor theme](https://packages.debian.org/en/source/sid/dmz-cursor-theme) (fallback cursor set on all themes)
* GTK 3 theme needs GTK+ >=3.24
* GTK 2 theme needs the industrial, mist and the and pixbuf/pixmap engine(with SVG support).

### Howto install/update?

#### Devuan/Debian (exporting the path is a [new Frankenstein thing](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=918754) of Debian >=10)

```
su -c 'PATH=/usr/sbin:/sbin:/usr/local/sbin:$PATH apt install fakeroot git libfile-fcntllock-perl debhelper --no-install-recommends'
cd /tmp && git clone --depth 1 https://gitlab.com/sixsixfive/ceres-gtk.git
cd ceres-gtk/packaging
sh build_deb.sh
su -c 'PATH=/usr/sbin:/sbin:/usr/local/sbin:$PATH dpkg -P ceres-theme-gtk;dpkg -i ceres-theme-gtk_*.deb;apt install -f --no-install-recommends'
```

##### with sudo installed(eg: LMDE or *buntu)

```
sudo apt install fakeroot git libfile-fcntllock-perl debhelper --no-install-recommends
cd /tmp && git clone --depth 1 https://gitlab.com/sixsixfive/ceres-gtk.git
cd ceres-gtk/packaging
sh build_deb.sh
sudo sh -c 'dpkg -P ceres-theme-gtk;dpkg -i ceres-theme-gtk_*.deb;apt install -f --no-install-recommends'
```

#### SuSE or Gecko Linux

```
sudo zypper install fakeroot git rpmbuild
cd /tmp && git clone depth -1 https://gitlab.com/sixsixfive/ceres-gtk.git
cd ceres-gtk/packaging
sh build_rpm.sh
sudo zypper install --no-recommends ceres-theme-gtk*.rpm
```

#### Any other

* 1: Download and extract the theme
```
cd /tmp && curl -L https://gitlab.com/sixsixfive/ceres-gtk/-/archive/master/ceres-gtk-master.tar.gz |tar zxf -
```
* 2: Copy the content of 'ceres-gtk' to your GTK/Metacity theme dirs (usually $SYSPREFIX/share/themes)
```
cp -R ceres-gtk-master/ceres-gtk/* /usr/local/share/themes
```
* 3: Copy the 'ceres-gtk-icons' folder to your icon theme dir (usually $SYSPREFIX/share/icons)
```
cp -R ceres-gtk-master/ceres-gtk-icons /usr/local/share/icons/
```

__NOTE:__ *The icon theme is needed for the GTK3 theme to display arrows, checkboxes and such stuff, it has to be installed systemwide since GTK is unable to load icons fom the home dir!*

### FAQ

#### Howto disable the GTK+3 overlay scrollbars in X11

#### Enable the CSDs for dialogs eg: file open or about dialogs

```
if [ ! -f ~/.config/gtk-3.0/settings.ini ]; then printf '[Settings]' >> ~/.config/gtk-3.0/settings.ini ;fi && printf '\ngtk-dialogs-use-header=1\n' >> ~/.config/gtk-3.0/settings.ini
```

#### Window border are too thin to resize the window?

Just use ALT+Mouse 2 to resize windows.

#### Howto set the GTK themes manually eg: on IceWM


for GTK:

```
printf 'include "/usr/share/themes/ceres/gtk/gtkrc"' >> ~/.gtkrc
```
or
```
printf 'include "/usr/share/themes/ceres-pro/gtk/gtkrc"' >> ~/.gtkrc
```

for GTK+2:

```
printf 'gtk-theme-name="ceres"' >> ~/.gtkrc-2.0
```
or
```
printf 'gtk-theme-name="ceres@2"' >> ~/.gtkrc-2.0
```
or
```
printf 'gtk-theme-name="ceres-pro"' >> ~/.gtkrc-2.0
```
or
```
printf 'gtk-theme-name="ceres-pro@2"' >> ~/.gtkrc-2.0
```

for GTK+3

```
if [ ! -f ~/.config/gtk-3.0/settings.ini ]; then printf '[Settings]' >> ~/.config/gtk-3.0/settings.ini ;fi && printf '\ngtk-theme-name=ceres-nocomposite\n' >> ~/.config/gtk-3.0/settings.ini
```
or
```
if [ ! -f ~/.config/gtk-3.0/settings.ini ]; then printf '[Settings]' >> ~/.config/gtk-3.0/settings.ini ;fi && printf '\ngtk-theme-name=ceres-pro-nocomposite\n' >> ~/.config/gtk-3.0/settings.ini
```

#### How to add more buttons to the titlebar?

##### Muffin

```
gsettings set org.cinnamon.desktop.wm.preferences button-layout "menu:shade,stick,above,minimize,maximize,close"
```

##### Marco

```
gsettings set org.mate.Marco.general button-layout "menu:shade,stick,above,minimize,maximize,close"
```

#### Print preview does not work?

Since this theme focusing on Cinnamon and MATE it will use [Atril](https://github.com/mate-desktop/atril) as the default document viewer.
to change this simple remove/comment the "gtk-print-preview-command" in the GTK3 settings.ini's.

#### Cinnamon theme is flatter than the GTK theme?

Cinnamon does not support full theming with complex gradients and multiple box shadows so I had to use a more flat theme.

#### Why does the muffin/metacity theme does not use the gtk theme color anymore?

The theme was rewritten to use SVGs since the old method won't work with scaling!

#### GTK2 HiDPI themes and font

To change the font dpi do the following:

##### XServer (no xsettings)

```
printf "Xft.dpi: 192\n" >>~/.Xresources
```

*if you want to change it back just remove or replace 192 wth 96!*
